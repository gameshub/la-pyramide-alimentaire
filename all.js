class Category{

    /**
     * Constucteur des catégories permettant de séparer les aliments
     * @param name
     * @param description
     * @param level
     */
    constructor(name, description, level) {
        this._name = name;
        this._description = description;
        this._level = level;
    }


    getname() {
        return this._name;
    }

    setname(value) {
        this._name = value;
    }

    getdescription() {
        return this._description;
    }

    setdescription(value) {
        this._description = value;
    }

    getlevel() {
        return this._level;
    }

    setlevel(value) {
        this._level = value;
    }
}
/**
 * @interface
 * @classdesc Interface Asset, défini un asset, soit un élément du canvas comme un bouton ou une grille
 * @author Vincent Audergon
 * @version 1.0
 */
class Asset {

    /**
     * Retourne la liste des éléments PIXI d'un asset
     * @return {Object[]} les éléments PIXI
     */
    getPixiChildren(){}

    /**
     * Retourne la position y du composant
     * @return {number} posY
     */
    getY(){}
    /**
     * Retourne la position x du composant
     * @return {number} posX
     */
    getX(){}
    setY(y){}
    setX(x){}
    getWidth(){}
    getHeight(){}
    setVisible(visible){}

}
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class Button extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {int} bgColor La couleur du bouton
     * @param {int} fgColor La couleur du texte
     */
    constructor(x, y, label, bgColor, fgColor, autofit = false, width = 150) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;
        /** @type {int} la couleur de fond du bouton */
        this.bgColor = bgColor;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
        this.lbl = new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 18, fill: fgColor, align: 'center'})

        this.height = 0;
        this.autofit = autofit;
        this.width = width;
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
        this.init();
    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
    }


    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        this.lbl.text = text;
        this.update();
    }

    update(){
        let buttonWidth = this.getWidth();
        let buttonHeight = this.lbl.height * 1.5;
        this.height = buttonHeight;
        this.graphics.clear();
        this.graphics.beginFill(this.bgColor);
        this.graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
        this.graphics.endFill();
        this.lbl.anchor.set(0.5);
        this.lbl.x = this.x;
        this.lbl.y = this.y;
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.graphics, this.lbl];
    }


    updateFont(font) {
        this.lbl.style.fontFamily = font;
    }

    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        for (let element of this.getPixiChildren()) {
            element.visible = visible
        }
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getWidth() {
        return this.autofit ? (this.width < this.lbl.width + 20 ? this.lbl.width + 20 : this.width) : this.width;
    }
}
class Cell extends Asset {


    /**
     * Constucteur des cellules servant à placer les produits en début de partie
     * @param x
     * @param y
     * @param size
     * @param isFull
     */
    constructor(x, y, size, isFull){
        super();
        this.x = x;
        this.y = y;
        this.size = size;
        this.isFull = isFull;

        this.container = new PIXI.Container();

    }


    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.size;
    }

    getHeight() {
        return this.size;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }


    getisFull() {
        return this.isFull;
    }

    setisFull(value) {
        this.isFull = value;
    }
}
class CheckBox extends Asset {

    /**
     * Créé le composant graphic
     */
    constructor(x = 0, y = 0, text = '', sizeFactor=1) {
        super();
        this.y = y;
        this.x = x;
        this.text = text;
        this.sizeFactor = sizeFactor;

        this.selected = false;

        this.elements = {
            square: new PIXI.Graphics(),
            line1: new PIXI.Graphics(),
            line2: new PIXI.Graphics(),
            text: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'left', breakWords:true, wordWrap:true})
        };

        this.elements.square.interactive = true;
        this.elements.square.buttonMode = true;
        this.elements.square.on('pointerdown', function () {
            this.select();
        }.bind(this));

        this.draw();
    }


    /**
     * Modifie la position de la checkbox.
     * @param x {number} Position X
     * @param y {number} Position Y
     */
    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.draw();
    }

    setText(value) {
        this.text = value;
        this.draw();
    }

    /**
     * Affiche ou cache le composant.
     * @param visible {boolean} Est visible ?
     */
    setVisible(visible) {
        if (!visible)
            for (let element of this.getPixiChildren())
                element.visible = false;
        else {
            this.elements.text.visible = true;
            this.elements.square.visible = true;
            this.select(false);
        }
    }

    draw() {
        let line1StartX = this.x + 3*this.sizeFactor;
        let line1StartY = this.y + 20*this.sizeFactor;
        let line1EndX = this.x + 13*this.sizeFactor;
        let line1EndY = this.y + 30*this.sizeFactor;

        let line2StartX = line1EndX-5*this.sizeFactor/4;
        let line2StartY = line1EndY+5*this.sizeFactor/4;
        let line2EndX = this.x + 35*this.sizeFactor;
        let line2EndY = this.y + 8*this.sizeFactor;


        this.elements.square.clear();
        this.elements.square.lineStyle(1, 0x000000, 1);
        this.elements.square.beginFill(0xe0e0e0, 0.25);
        this.elements.square.drawRoundedRect(this.x, this.y, 38*this.sizeFactor, 38*this.sizeFactor, 5);
        this.elements.square.endFill();

        this.elements.line1.clear();
        this.elements.line1.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line1StartX, line1StartY).lineTo(line1EndX, line1EndY);

        this.elements.line2.clear();
        this.elements.line2.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line2StartX, line2StartY).lineTo(line2EndX, line2EndY);


        this.elements.text.text = this.text;
        this.elements.text.style.wordWrapWidth =600-Math.floor( this.x + 5 + 38*this.sizeFactor +25);
        this.elements.text.anchor.set(0.0);
        this.elements.text.x = this.x + 38*this.sizeFactor+5;
        this.elements.text.y = this.y + (38*this.sizeFactor-this.elements.text.height)/2;

    }

    getPixiChildren() {
        let objects = [];
        for (let element of Object.keys(this.elements)) {
            objects.push(this.elements[element]);
        }
        return objects;
    }

    select(toggle = true) {
        if (toggle)
            this.selected = !this.selected;
        this.elements.line1.visible = this.selected;
        this.elements.line2.visible = this.selected;
    }

    isChecked() {
        return this.selected;
    }


    updateFont(font){
        this.elements.text.style.fontFamily = font;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }


    setY(y) {
        this.y = y;
        this.setPosition(this.x, this.y);
    }

    setX(x) {
        this.x = x;
        this.setPosition(this.x, this.y);
    }
}
class Circle extends Asset {


    /**
     * Constucteur des cercles indiquant quels produits est bien ou mal placés
     * @param x
     * @param y
     * @param bgImage
     * @param size
     */
    constructor(x, y, bgImage, size){
        super();
        this.x = x;
        this.y = y;
        this.bgImage = bgImage;
        this.size = size;

        this.container = new PIXI.Container();

        this.init();

    }

    init(){
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        //Créer un "Sprite" avec une image en base64.
        //Le type d'image est "IMG" donc pour récupérer l'image, il faut faire
        //this.image.image.src
        this.shape = PIXI.Sprite.fromImage(this.bgImage.image.src);

        //Définir sa position et l'ancre sur l'image, ici au centre de l'image
        this.shape.anchor.x = 0.5;
        this.shape.anchor.y = 0.5;
        this.shape.x = this.x;
        this.shape.y = this.y;
        this.shape.width = this.size;
        this.shape.height = this.size;

        this.container.addChild(this.shape);
    }


    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.size;
    }

    getHeight() {
        return this.size;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }


}
/**
 * @classdesc Asset grille de dessin, retourne une séries de points lorsque l'utilisateur a dessiné un polygone
 * @author Vincent Audergon
 * @version 2.0
 */
class DrawingGrid extends Asset {

    /**
     * Constructeur de la grille de dessin
     * @param {int} col Le nombre de colonnes qui composent la grille
     * @param {int} lines Le nombre de lignes qui composent la grille
     * @param {double} width La largeur / hauteur entre chaque noeuds de la grille
     * @param {Pixi.Stage} stage Le stage Pixi
     * @param {function} onShapeCreated La fonction de callback appelée lorsqu'une forme a été dessinée
     */
    constructor(col, lines, width, stage, onShapeCreated) {
        super();
        /** @type {int} le nombre de colonnes */
        this.col = col;
        /** @type {int} le nombre de lignes */
        this.lines = lines;
        /** @type {double} la largeur / hauteur d'une cellule */
        this.width = width;
        /** @type {PIXI.Stage} le stage PIXI sur lequel dessiner les éléments de la grille */
        this.stage = stage;
        /** @type {Node[]} la liste des noeuds qui composent la grille */
        this.nodes = [];
        /** @type {Point[]} la liste des points de la forme en cours de dessin */
        this.points = [];
        /** @type {PIXI.Graphics} la liste des lignes dessinées */
        this.strLines = [];
        /** @type {Point} l'origine de la forme en cours de dessin */
        this.origin = new Point(0, 0);
        /** @type {Node} le dernier noeud rencontré lors du dessin */
        this.lastnode = undefined;
        /** @type {PIXI.Graphics} le trait qui suit le curseur lors d'un dessin */
        this.line = undefined;
        /** @type {function} fonction de callback appelée lorsqu'une forme est dessinée */
        this.onShapeCreated = onShapeCreated;
        /** @type {boolean} si un dessin est en cours */
        this.drawing = false;
        this.init();
    }

    /**
     * Initialise la grille de dessin
     */
    init() {
        this.stage.interactive = true;
        this.stage.on('pointermove', this.onPointerMove.bind(this));
        this.stage.on('pointerup', this.onReleased.bind(this));
        for (let c = 0; c < this.col; c++) {
            for (let l = 0; l < this.lines; l++) {
                this.nodes.push(new Node(c * this.width, l * this.width, this.width / 4.5, this));
            }
        }
    }

    /**
     * Réinitialise la grille de dessin
     */
    reset() {
        for (let line of this.strLines) {
            this.stage.removeChild(line);
        }
        this.strLines = [];
        this.points = [];
        this.origin = new Point(0, 0);
        this.drawing = false;
    }

    /**
     * Créer le prochain point du polygone en cours de dessin
     * @param {Node} node le {@link Node} qui défini le nouveau point
     * @return {Point} le nouveau {@link Point}
     */
    createNextPoint(node) {
        let p = new Point(node.x / this.width - this.origin.x, node.y / this.width - this.origin.y);
        p.name = String.fromCharCode(65 + this.points.length);
        return p;
    }

    /**
     * Vérifie qu'un point ne soit pas deja existant dans la grille
     * @param {Point} p le point à controller
     * @return {boolean} si le point existe déjà
     */
    containsPoint(p) {
        for (let point of this.points) {
            if (point.x === p.x && point.y === p.y) return true;
        }
        return false;
    }

    /**
     * Créer le trait affiché sur la grille lorsque l'utilisateur déssine
     * @param {Node} node le noeud de départ
     */
    initLine(node) {
        this.line = new PIXI.Graphics();
        this.stage.addChild(this.line);
        this.line.lineStyle(6, 0xFF0000, 1);
        this.line.moveTo(node.center().x, node.center().y);
    }

    /**
     * Dessine une ligne le dernier noeud rencontré et le noeud donné en argument
     * @param {Node} node le noeud jusqu'au quel faire le trait
     */
    drawStrLine(node) {
        this.stage.removeChild(this.line);
        this.initLine(node);
        let straightLine = new PIXI.Graphics();
        this.strLines.push(straightLine);
        this.stage.addChild(straightLine);
        straightLine.lineStyle(6, 0xFF, 1);
        straightLine.moveTo(this.lastnode.center().x, this.lastnode.center().y);
        straightLine.lineTo(node.center().x, node.center().y);
        this.lastnode = node;
        if (this.onLineDrawn) this.onLineDrawn();
    }

    /**
     * Retourne la liste des éléments PIXI de la grille de dessin
     * @return {Object[]} les éléments PIXI qui composent la grille
     */
    getPixiChildren() {
        let children = [];
        for (let n of this.nodes) {
            if (n.graphics) children.push(n.graphics);
            if(n.point) children.push(n.point);
        }
        return children;
    }

    /**
     * Méthode de callback appelée lorsqu'un click est effectué sur un noeud de la grille.
     * Défini le noeud comme étant le noeud de départ et crée le trait de dessin
     * @param {Event} e l'événement JavaScript
     * @param {Node} node le noeud sur lequel on a clické
     */
    onNodeClicked(e, node) {
        this.lastnode = node;
        this.initLine(node);
        this.drawing = true;
        this.points.push();
        this.origin = this.createNextPoint(node);
        let p = new Point(0, 0);
        p.name = this.origin.name;
        this.points.push(p);
    }

    /**
     * Méthode de callback appelée lorsque le click est relâché
     * @param {Event} e L'événement JavaScript
     */
    onReleased(e) {
        this.drawing = false;
        this.stage.removeChild(this.line);
        this.reset();
    }

    /**
     * Méthode de callback appelée lorsque le curseur bouge.
     * Déssine le trait à la position du curseur
     * @param {Event} e L'événement JavaScript
     */
    onPointerMove(e) {
        if (this.drawing) {
            let position = e.data.getLocalPosition(this.stage);
            this.line.lineTo(position.x, position.y);
            for (let n of this.nodes) {
                if (n.graphics.containsPoint(e.data.getLocalPosition(n.graphics.parent))) this.onNodeEncountered(e, n);
            }
        }
    }

    /**
     * Méthode de callback appelée lorsque que le curseur touche un noeud de la grille.
     * Vérifie si la forme est terminée ou si il faut dessiner un trait droit entre ce noeud et le noeud de départ.
     * @param {Event} e L'événement JavaScript
     * @param {Node} node Le noeud de la grille touché
     */
    onNodeEncountered(e, node) {
        if (this.drawing && this.lastnode !== node) { // Si le noeud rencontré n'est pas le dernier noeud
            let point = this.createNextPoint(node);
            this.drawStrLine(node);
            let len = this.points.length;
            // console.log("Diagonale : ");
            // if (len >= 2) {
            //     let val1 = this.points[len - 2].sub(this.points[len - 1]);
            //     let val2 =  this.points[len - 1].sub(point);
            //     console.log("############################################################");
            //     console.log("Point len - 2 : ");
            //     console.log(this.points[len - 2]);
            //     console.log("Point len - 1 : ");
            //     console.log(this.points[len - 1]);
            //     console.log("Point :");
            //     console.log(point);
            //     console.log("len-2 - len-1 :");
            //     console.log(val1);
            //     console.log("len-1 - len :");
            //     console.log(val2);
            //
            //     if ((this.points[len - 1].x === this.points[len - 2].x && this.points[len - 1].x === point.x) || //Al. horizontal
            //         (this.points[len - 1].y === this.points[len - 2].y && this.points[len - 1].y === point.y) || //Al. vertical
            //         (val1.equals(val2))) { //Al. diagonale
            //         //Si trois points sont allignés on ne crée pas un nouveau mais on déplace le dernier
            //         if (point.equals(this.points[0]) && len >= 3) {
            //             //Si c'est le dernier point, on efface le dernier
            //             this.points.pop();
            //             this.pointsCalcul.push(point);
            //         } else if (!this.containsPoint(point)) {
            //             //Si c'est pas le dernier et qu'il n'exite pas encore on déplace le dernier
            //             point.name = this.points[len - 1].name;
            //             this.points[len - 1] = point;
            //             this.pointsCalcul.push(point);
            //         }
            //     } else if (!this.containsPoint(point)) {
            //         this.points.push(point);
            //         this.pointsCalcul.push(point);
            //     }
            // }
            if (!this.containsPoint(point)) {
                this.points.push(point);
            }
            if (this.points.length >= 3 && point.equals(this.points[0])) { //Sinon, si il correspond à l'origine
                //Fin de la forme

                this.drawStrLine(node);
                this.onShapeCreated(new Shape(0, 0, this.points, this.width, 'unknown', {}, this.points[0]));
                this.reset();
            }
        }
    }

    /**
     * Défini la fonction de callback appelée lorsqu'une ligne est dessinée
     * @param {function} onLineDrawn La fonction de callback
     */
    setOnLineDrawn(onLineDrawn) {
        this.onLineDrawn = onLineDrawn;
    }

    /**
     * Défini la fonction de callback lorsqu'un polygone est créé sur le canvas
     * @param {function} onShapeCreated La fonction de callback
     */
    setOnShapeCreated(onShapeCreated) {
        this.onShapeCreated = onShapeCreated;
    }

}
class Product extends Asset {


    /**
     * Constucteur des produits affichés pendant le jeu
     * @param x
     * @param y
     * @param width
     * @param height
     * @param image
     * @param imageName
     * @param name
     * @param kCal
     * @param category
     * @param advice
     * @param recommandation
     * @param isPlaced
     * @param isWellPlaced
     * @param onMove
     * @param onDBClick
     */
    constructor(x, y, width, height, image, imageName, name, kCal, category, advice, recommandation, isPlaced, isWellPlaced, onMove, onDBClick) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.image = image;
        this.imageName = imageName;
        this.name = name;
        this.kCal = kCal;
        this.category = category;
        this.advice = advice;
        this.recommandation = recommandation;
        this.isPlaced = isPlaced;
        this.isWellPlaced = isWellPlaced;
        this.onMove = onMove;
        this.onDBClick = onDBClick;

        this.container = new PIXI.Container();
        this.isFirstClick = true;
        this.data = undefined;
        this.shape = undefined;
        this.display();
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        //Créer un "Sprite" avec une image en base64.
        //Le type d'image est "IMG" donc pour récupérer l'image, il faut faire
        //this.image.image.src
        this.shape = PIXI.Sprite.fromImage(this.image.image.src);

        //Définir sa position et l'ancre sur l'image, ici au centre de l'image
        this.shape.anchor.x = 0.5;
        this.shape.anchor.y = 0.5;
        this.shape.x = this.x;
        this.shape.y = this.y;
        this.shape.width = this.width;
        this.shape.height = this.height;

        //Ajouter les listeners
        this.shape.interactive = true;
        this.shape.on('pointerdown', this.onClick.bind(this));
        this.shape.on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));

        //Ajouter notre forme sur l'élément graphique
        this.container.addChild(this.shape);
    }

    onClick() {
        if (this.isFirstClick) {
            this.isFirstClick = false;
            //Annulation du double clique après 500ms
            setTimeout(function () {
                this.isFirstClick = true;
            }.bind(this), 500);
        } else {
            this.isFirstClick = true;
            if (this.onDBClick)
                this.onDBClick(this);
        }
    }

    onDragStart(event) {
        if(this.onMove == false){

        } else{
            this.data = event.data;
            this.previousX = this.x;
            this.previousY = this.y;
        }
    }


    onDragEnd() {
        this.data = null;
        //Appel au callback s'il y en a un
        if (this.onMove) {
            this.onMove(this);
        }
    }

    onDragMove() {
        if (this.data) {
            //Position actuelle
            let newPosition = this.data.getLocalPosition(this.shape.parent);

            //Empêcher la forme de sortir du canvas et de devenir invisible
            newPosition.x = newPosition.x < 0 ? 0 : newPosition.x;
            newPosition.y = newPosition.y < 0 ? 0 : newPosition.y;
            newPosition.x = newPosition.x > 600 ? 600 : newPosition.x;
            newPosition.y = newPosition.y > 600 ? 600 : newPosition.y;

            //Mise à jour la position de la forme
            this.shape.x = newPosition.x;
            this.shape.y = newPosition.y;
            this.y = newPosition.y;
            this.x = newPosition.x;

        }
    }


    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        //Mise à jour de l'affichage
        this.display();
    }

    setX(x) {
        this.x = x;
        //Mise à jour de l'affichage
        this.display();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    getImageName() {
        return this.imageName;
    }

    getName() {
        return this.name;
    }

    getKcal() {
        return this.kCal;
    }

    getCategory() {
        return this.category;
    }

    getAdvice() {
        return this.advice;
    }

    getRecommandation() {
        return this.recommandation;
    }

    getisPlaced(){
        return this.isPlaced;
    }

    setisPlaced(isPlaced){
        this.isPlaced = isPlaced;
    }

    getisWellPlaced(){
        return this.isWellPlaced;
    }

    setisWellPlaced(isWellPlaced){
        this.isWellPlaced = isWellPlaced;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }
}
class Pyramid extends Asset {


    /**
     * Constucteur de la pyramide utilisée pendant le jeu
     * @param x
     * @param y
     * @param width
     * @param height
     * @param imgBackground
     */
    constructor(x, y, width, height, imgBackground) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.levelHeight = height / 6;
        this.imgBackground = imgBackground;

        this.container = new PIXI.Container();

        this.init();
    }


    init(){
        this.container.removeChildren();

        let bg = PIXI.Sprite.fromImage(this.imgBackground);
        bg.x = this.x;
        bg.y = this.y;
        bg.width = this.width;
        bg.height = this.height;
        bg.anchor.x = 0.5;
        bg.anchor.y = 0.5;

        this.container.addChild(bg);
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    getLevelHeight(){
        return this.levelHeight;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    get isFull() {
        return this.isFull;
    }

    set isFull(value) {
        this.isFull = value;
    }

}
class ScrollPane extends Asset {


    constructor(x, y, width, height) {
        super();
        this.x = x | 0;
        this.y = y | 0;
        this.width = width | 0;
        this.height = height | 0;
        this.index = 1;

        this.maxIndex = 0;

        this.touchScrollLimiter = 0;
        this.lastTouch = 0;
        this.data = {};
        this.dragging = false;

        this.elements = [];

        this.graphics = {
            elementContainer: new PIXI.Container(),
            scrollBar: new PIXI.Graphics(),
            scrollButton: new PIXI.Graphics()
        };

        this.graphics.scrollButton
            .on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));


        let canvas = document.getElementById('canvas');
        canvas.addEventListener('wheel', function (event) {
            this.index += (event.deltaY > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length));
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }.bind(this));
        canvas.addEventListener('touchmove', function (event) {
            if (this.lastTouch === 0)
                this.lastTouch = event.touches[0].clientY;
            this.touchScrollLimiter += (this.lastTouch - event.touches[0].clientY);
            this.lastTouch = event.touches[0].clientY;
            if ((this.touchScrollLimiter > (this.maxIndex / this.elements.length))
                || (this.touchScrollLimiter < -(this.maxIndex / this.elements.length))) {
                console.log(this.touchScrollLimiter);
                this.index += this.touchScrollLimiter > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length);
                this.touchScrollLimiter = 0;
                if (this.index > this.height - 31)
                    this.index = (this.height - 31);
                else if (this.index <= 1)
                    this.index = 1;
                this.scroll();
            }
        }.bind(this));
        canvas.addEventListener('touchend', function (event) {
            this.lastTouch = 0;
        }.bind(this));
    }

    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.init();
    }

    addElements(...elements) {
        for (let element of elements)
            this.elements.push(element);
    }

    init() {

        console.log(this.elements);

        this.graphics.scrollBar.clear();
        this.graphics.scrollBar.beginFill(0xDDDDDD);
        this.graphics.scrollBar.drawRect(this.x + this.width - 18, this.y, 18, this.height);
        this.graphics.scrollBar.endFill();

        this.graphics.scrollButton.clear();
        this.graphics.scrollButton.beginFill(0x999999);
        this.graphics.scrollButton.drawRect(this.x + this.width - 17, this.y + 1, 16, 30);
        this.graphics.scrollButton.endFill();

        this.graphics.scrollButton.interactive = true;
        this.graphics.scrollButton.buttonMode = true;

        let y = this.y;
        for (let element of this.elements) {
            if (element instanceof Asset) {
                element.setY(y);
                element.setX((this.width-20) / 2);
                y += element.getHeight();
                element.setVisible(this.checkIsInContainer(element.getY(), element.getHeight() / 2));
                for (let pc of element.getPixiChildren())
                    this.graphics.elementContainer.addChild(pc);
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                element.y = y;
                y += (typeof element.height !== 'undefined' ? element.height : 25);
                element.visible = this.checkIsInContainer(element.y);
                this.graphics.elementContainer.addChild(element);
                element.x = this.width / 2;
            }
            y += 5;
        }
        this.maxIndex = y;

        this.graphics.scrollButton.visible = (this.height < this.maxIndex);
        this.graphics.scrollBar.visible = (this.height < this.maxIndex);

        this.scroll();
    }

    clear() {
        this.elements = [];
    }

    scroll() {
        if (this.elements.length === 0)
            return;

        this.graphics.scrollButton.position.y = this.index;

        let firstDisplayedIndex = Math.floor((this.elements.length - 1) * (this.index / this.height));

        let posY = 1 + this.y + this.elements[firstDisplayedIndex].getHeight()/2;

        for (let i = 0; i < this.elements.length; i++) {
            let element = this.elements[i];
            if (element instanceof Asset) {
                if (i < firstDisplayedIndex)
                    element.setVisible(false);
                else {
                    element.setY(posY);
                    element.setVisible(this.checkIsInContainer(element.getY()));
                    posY += element.getHeight() + 5;
                }
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                if (i < firstDisplayedIndex)
                    element.visible = false;
                else {
                    element.y = posY;
                    element.visible = this.checkIsInContainer(element.y);
                    posY += element.height + 5;
                }
            }
            if(element instanceof ToggleButton)
                element.updateView();
        }
    }


    checkIsInContainer(posY, compensation = 0) {
        return (posY < (this.y + this.height) && (posY > (this.y + compensation)));
    }

    getPixiChildren() {
        let elements = [];
        for (let e in this.graphics)
            if (this.graphics[e] instanceof Asset)
                for (let element of this.graphics[e].getPixiChildren())
                    elements.push(element);
            else
                elements.push(this.graphics[e]);
        return elements;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {

    }

    onDragStart(event) {
        this.data = event.data;
        this.graphics.scrollButton.alpha = 0.5;
        this.dragging = true;
    }

    onDragEnd() {
        this.graphics.scrollButton.alpha = 1;
        this.dragging = false;
        this.data = null;
    }

    onDragMove() {
        if (this.dragging) {
            let newPosition = this.data.getLocalPosition(this.graphics.scrollButton.parent);
            this.index = newPosition.y - this.y;
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }
    }
}
class Select extends Asset{


    constructor(height, width, x, y) {
        super();

        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;

        this.button = new Button(x,y,'SELECT',0xFFFFFF,0x000000, false,width);
        this.container = new PIXI.Container();

        this.currentIndex = 0;
        this.elements = [];

        this.button.setOnClick(function () {
            this.container.visible = !this.container.visible;
        }.bind(this));

        this.container.visible = true;
    }

    addElement(element){
        element.setSelect(this);
        this.elements.push(element);
        this.build();
    }


    onClick(data){
        console.log(data);
    }

    build(){
        this.container.removeChildren(0);
        this.container.width = this.width;
        this.container.height = this.height;
        this.container.x = this.x-this.width/2;
        this.container.y = this.y+30;


        let currentY = 0;
        for(let element of this.elements){
            element.build(this.width/2, currentY, this.width);
            currentY+=element.getHeight();
            for(let child of element.getPixiChildren()){
                this.container.addChild(child);
            }
        }


    }


    resize(height, width){
        this.height = height;
        this.width = width;
        this.build();
    }

    setPosition(x,y){
        this.x = x;
        this.y = y;
        this.build();
    }

    getPixiChildren() {
        let elements = [];
        for (let e of this.button.getPixiChildren()){
            elements.push(e);
        }
        elements.push(this.container);
        return elements;
    }



    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        this.button.setVisible(visible);
        this.container.visible = visible;
    }
}
class SelectItem extends Asset{

    constructor(text, data=null) {
        super();
        this.selectRef = null;
        this.button = null;
        this.text = text;
        this.data = data;
        this.build(0,0,100);
    }


    build(x,y,width){
        this.button = new Button(x,y,this.text,0xFFFFFF,0x000000, false,width);
        this.button.setOnClick(function () {
            this.selectRef.onClick(this.data);
        }.bind(this));
    }


    getPixiChildren() {
        return this.button ? this.button.getPixiChildren():[];
    }

    setText(text){
        this.text = text;
    }

    setSelect(ref){
        this.selectRef = ref;
    }

    getData(){
        return data;
    }

    setData(data){
        this.data = data;
    }

    getHeight(){
        return this.button != null ? this.button.getHeight() : 0;
    }


    getY() {
        this.button.getY();
    }

    getX() {
        this.button.getX();
    }

    setVisible(visible) {
        this.button.setVisible(visible);
    }
}
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class ToggleButton extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {boolean} fitToText Taille du bouton en fonction du texte
     * @param {number} width Taille forcée du bouton
     */
    constructor(x, y, label, fitToText=false, width = 150) {
        super();

        this.fitToText = fitToText;
        this.refToggleGroup = null;
        this.width = width;
        this.height = 0;
        this._data = null;
        this.currentState = 'inactive';
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;

        this.lastOnClick = function(){
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };

        this.states = {
            active: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0xFFFFFF, align: 'center' })
            },
            inactive: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'center' })
            },
            disabled: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x666666, align: 'center' })
            }
        };

        this.onClick = this.lastOnClick;
        this.init();
        this.updateView();

    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        for (let state of Object.keys(this.states)) {
            this.states[state].graphics.interactive = true;
            this.states[state].graphics.buttonMode = true;
            this.states[state].graphics.on('pointerdown', function () {
                this.onClick()
            }.bind(this));
        }
    }

    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        for (let state of Object.keys(this.states)){
            this.states[state].lbl.text = text;
        }
        this.update();
    }


    update(){
        for (let state of Object.keys(this.states)){
            let buttonWidth = this.fitToText ? (20 + this.states[state].lbl.width): this.width;
            let buttonHeight = this.states[state].lbl.height * 1.5;
            this.height = buttonHeight;
            this.states[state].graphics.clear();
            if(state === 'active')
                this.states[state].graphics.beginFill(0x00AA00);
            else if(state === 'inactive')
                this.states[state].graphics.beginFill(0xDDDDDD);
            else
                this.states[state].graphics.beginFill(0xCCCCCC);
            this.states[state].graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
            this.states[state].graphics.endFill();
            this.states[state].lbl.anchor.set(0.5);
            this.states[state].lbl.x = this.x;
            this.states[state].lbl.y = this.y;
        }
    }

    toggle(){
        this.currentState = (this.currentState === 'active') ? 'inactive' : 'active';
        this.updateView();
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = function () {
            onClick(this);
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };
        this.lastOnClick = this.onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.states.inactive.graphics,
            this.states.inactive.lbl,
            this.states.active.graphics,
            this.states.active.lbl,
            this.states.disabled.graphics,
            this.states.disabled.lbl];
    }

    updateView(){
        for (let state of Object.keys(this.states)){
            if(state === this.currentState){
                this.states[state].lbl.visible = true;
                this.states[state].graphics.visible = true;
            } else{
                this.states[state].lbl.visible = false;
                this.states[state].graphics.visible = false;
            }
        }
    }

    isActive(){
        return this.currentState === 'active';
    }

    isEnabled(){
        return this.currentState !== 'disabled';
    }

    set data(data){
        this._data = data;
    }

    get data(){
        return this._data;
    }


    show(){
        this.updateView();
    }

    hide(){
        for (let element of this.getPixiChildren()){
            element.visible = false;
        }
    }

    disable(){
        this.currentState = 'disabled';
        this.onClick = function () {};
        this.updateView();
    }

    enable(){
        this.currentState = 'inactive';
        this.onClick = this.lastOnClick;
        this.updateView();
    }

    setRefToggleGroup(ref){
        this.refToggleGroup = ref;
    }

    updateFont(font){
        for(let state of Object.keys(this.states)){
            this.states[state].lbl.style.fontFamily = font;
        }
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        visible = true;
        console.log(this.currentState);
        for (let element of this.getPixiChildren()){
            element.visible = visible;
        }
    }


    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getHeight() {
        return this.height;
    }

    getWidth() {
        return this.width;
    }
}
class ToggleGroup extends Asset {

    /**
     *
     * @param mode
     * @param buttons
     */
    constructor(mode = 'single', buttons = []) {
        super();
        this.mode = mode;
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    addButton(button) {
        if (button != null) {
            button.setRefToggleGroup(this);
            this.buttons.push(button);
        }
    }

    setButtons(buttons = []) {
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    getButtons() {
        return this.buttons;
    }

    getActives() {
        let actives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'active')
                actives.push(btn);
        return actives;
    }

    getInactives() {
        let inactives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'inactive')
                inactives.push(btn);
        return inactives;
    }

    getPixiChildren() {
        let elements = [];
        for (let toggleButton of this.buttons)
            for (let element of toggleButton.getPixiChildren())
                elements.push(element);
        return elements;
    }

    updateList(button) {
        if (this.mode === 'single' && button.isActive()) {
            for (let btn of this.buttons) {
                if (btn !== button && btn.isActive()) {
                    btn.toggle();
                }
            }
        }
    }

    reset(){
        for (let btn of this.buttons){
            if(btn.isActive())
                btn.toggle();
        }
    }

    show() {
        for (let btn of this.buttons)
            btn.show();
    }

    hide() {
        for (let btn of this.buttons)
            btn.hide();
    }

    updateFont(font){
        for (let btn of this.buttons) {
            btn.updateFont(font);
        }
    }

    clearButtons(){
        this.buttons = [];
    }


    getY() {
        return 0;
    }

    getX() {
        return 0;
    }

    setVisible(visible) {
        for (let btn of this.buttons)
            btn.setVisible(visible);
    }
}

/**
 * @classdesc Définition d'une interface (ihm)
 * @author Vincent Audergon
 * @version 1.0
 */
class Interface {

    /**
     * Constructeur d'une ihm
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {string} scene le nom de la scène utilisée par l'ihm
     */
    constructor(refGame, scene) {
        this.refGame = refGame;
        /** @type {PIXI.Container} la scène PIXI sur laquelle dessiner l'ihm */
        this.scene = refGame.scenes[scene];
        /** @type {string[]} les textes de l'interface dans toutes les langues */
        this.texts = [];
        /** @type {Object[]} les éléments qui composent l'ihm */
        this.elements = [];
    }

    /**
     * Défini la scène de l'ihm
     * @param {PIXI.Container} scene la scène PIXI
     */
    setScene(scene) {
        this.scene = scene;
    }

    /**
     * Définis les éléments PIXI contenus dans la scène
     * @param {Objects} elements la liste des éléments PIXI
     */
    setElements(elements) {
        this.elements = elements;
    }

    /**
     * Défini la liste des textes de l'ihm dans la langue courante
     * @param {string[]} texts la liste des textes
     */
    setTexts(texts) {
        this.texts = texts;
    }

    /**
     * Retourne un texte de l'ihm dans la langue courante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key) {
        return this.texts[key];
    }

    /**
     * Fonction de callback appelée lors d'un changement de langue
     * @param {string} lang
     */
    refreshLang(lang) { }

    /**
     * Retire tous les éléments de l'ihm de la scène PIXI
     */
    clear() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.removeChild(c);
                }
            } else {
                this.scene.removeChild(el);
            }
        }
    }

    /**
     * Ajoute tous les éléments de l'ihm dans la scène PIXI
     */
    init() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.addChild(c);
                }
            } else {
                this.scene.addChild(el);
            }
        }
        this.refGame.showScene(this.scene);
    }

    /**
     * Affiche l'ihm
     */
    show() { }

}
/**
 * /**
 * @classdesc IHM pour le mode Créer
 * @author Julien Savoy
 * @version 1.0
 */
class Create extends Interface {

    /**
     * Consctucture de l'IHM Create
     * @param refGame
     */
    constructor(refGame) {

        super(refGame, "create");

    }

    /**
     * Point d'entrée pour l'affichage
     */
    show() {
        this.clear();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    /**
     * /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {

    }



}
/**
 * /**
 * @classdesc IHM pour le mode Explorer
 * @author Julien Savoy
 * @version 1.0
 */
class Explore extends Interface {

    /**
     * Consctucture de l'IHM Explorer
     * @param refGame
     */
    constructor(refGame) {
        super(refGame, "explore");

    }


    /**
     * Point d'entrée pour l'affichage
     */
    show() {
        this.setProductCells();
        this.clear();
        this.refGame.showText(this.refGame.global.resources.getTutorialText('explore'));
        let scenario = this.refGame.global.resources.getScenario();
        this.initGame(JSON.parse(scenario));
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }


    /**
     * Initie le début du jeu avec le scénario
     * @param scenario
     */
    initGame(scenario){
        this.activeProducts = [];

        let imgPyramid = this.refGame.global.resources.getImage("pyramide_vide_couleur").image.src;
        this.pyramid = new Pyramid(375,300,450,390, imgPyramid);
        this.activeProducts.push(this.pyramid);


        // Création des 6 catégories d'aliments
        this.categories = [];
        for (let i = 0; i < scenario.length; i++) {
            let categorie = scenario[i];
            this.categories.push(new Category(categorie.name, categorie.description, categorie.level));
        }

        this.getAllProducts(scenario);
        this.showExerciceProducts();
    }


    /**
     * Défini des cellules dans lesquelles seront placés les produits en début de partie
     */
    setProductCells(){
        this.cells = [];
        for (let i = 1; i < 7; i++) {
            this.cells.push(new Cell(35, 70 * i, 70, false))
        }
        for (let i = 1; i < 7; i++) {
            this.cells.push(new Cell(100, 70 * i, 70, false))
        }
        for (let i = 1; i < 4; i++) {
            this.cells.push(new Cell(175, 70 * i, 70, false))
        }
    }

    /**
     * Récupère tous les produits du scénario depuis la DB
     * @param scenario
     */
    getAllProducts(scenario){
        this.products = [];
        for(var i = 0; i < scenario.length; i++) {
            var obj = scenario[i];
            for(var j = 0; j < obj.products.length; j++) {
                var produit = obj.products[j];
                this.products.push(new Product(50, 50, 70, 70, this.refGame.global.resources.getImage(produit.imgName), produit.imgName, produit.name, produit.kCal, obj.level, produit.advice, produit.recommandation, false, false, this.onShapeMove.bind(this), this.onShapeClick.bind(this)));
            }
        }


    }

    /**
     * Affiche les produits présent dans l'exercice
     */
    showExerciceProducts(){
        this.data = this.refGame.global.resources.getExercice();

        let exercice = this.shuffle(JSON.parse(this.data.exercice));

        for(var i = 0; i < exercice.length; i++) {
            var ob = exercice[i];
            if(ob.category.localeCompare("Infos") != 0){
                for(var j = 0; j < ob.products.length; j++) {
                    var produit = ob.products[j];
                    for(var k = 0; k < this.products.length; k++) {
                        if(produit.name == this.products[k].getImageName()){
                            let product = this.placeProduct(this.products[k]);
                            this.activeProducts.push(product);
                        }
                   }
                }
            }
        }
        this.setElements(this.activeProducts);
    }

    /**
     * Méthode utilisée pour rendre aléatoire l'affichage des produits de l'exercice
     * @param array
     * @returns {*}
     */
    shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    /**
     * Change la position d'un produit pour le placer dans une cellule précédemment créé
     * @param product
     * @returns {*}
     */
    placeProduct(product){
        for (let i = 0; i < this.cells.length; i++) {
            if(this.cells[i].isFull == false){
                let cell = this.cells[i];
                product.setX(cell.getX());
                product.setY(cell.getY());
                cell.setisFull(true);
                break;
            }
        }
        return product;
    }

     /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {

    }


    /**
     * Affiche un pop-up avec toutes les informations sur l'aliment sélectionné
     * @param shape
     */
    onShapeClick(shape){
        if(shape.getCategory() == -1){
            this.refGame.global.util.showAlert('info',  "", shape.name);
        } else {
                this.refGame.global.util.showAlert('info',  shape.kCal + " " + this.refGame.global.resources.getOtherText('100_g') + ". " + shape.recommandation, shape.name, shape.advice);
        }
    }

    /**
     * Détecte sur quel niveau a été placé l'aliment et affiche une pop-up indiquant si le placement est correcte ou pas
     * S'il ne l'est pas, le produit reviens à sa dernière position
     * @param shape
     */
    onShapeMove(shape){
        let canvasHeight = this.refGame.global.canvas.HEIGHT;
        let canvasWidth = this.refGame.global.canvas.WIDTH;

        let leftMargin = (canvasWidth - this.pyramid.width);
        let bottomTopMargin = (canvasHeight - this.pyramid.height) / 2;
        let levelHeight = this.pyramid.getLevelHeight();

        let levelWidth = 37.5;
        let levelNumber = 5;

        // Boucle dans les catégories pour détecter sur quel niveau est présent le produit placé
        for (let i = 0; i < this.categories.length; i++) {
            if(shape.y < (canvasHeight - bottomTopMargin  - (levelHeight * i )) && shape.y > (bottomTopMargin + (levelHeight * (levelNumber - i))) && shape.x > leftMargin + (levelWidth * i) && shape.x < canvasWidth - (levelWidth * i)){
                if(shape.getCategory() == this.categories[i].getlevel()){
                    this.refGame.global.util.showAlert('success', this.refGame.global.resources.getOtherText('right_placement') + shape.name, this.refGame.global.resources.getOtherText('bravo'));
                } else {
                    this.refGame.global.util.showAlert('error', this.refGame.global.resources.getOtherText('product') + shape.name + this.refGame.global.resources.getOtherText('wrong_placement'), this.refGame.global.resources.getOtherText('error'));
                    shape.setX(shape.previousX);
                    shape.setY(shape.previousY);
                }
            }
        }
    }

}
/**
 * /**
 * @classdesc IHM pour le mode Créer
 * @author Julien Savoy
 * @version 1.0
 */
class Play extends Interface {

    constructor(refGame) {

        super(refGame, "play");

    }

    show() {
        this.clear();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    refreshLang(lang) {

    }

    refreshFont(isOpenDyslexic){

    }

}
/**
 * /**
 * @classdesc IHM pour les modes Entrainer & Evaluer
 * @author Julien Savoy
 * @version 1.0
 */
class Train extends Interface {

    /**
     * Consctucture de l'IHM Explorer
     * @param refGame
     */
    constructor(refGame) {

        super(refGame, "train");
        this.evaluate = false;
    }

    /**
     * Point d'entrée pour l'affichage
     */
    show(evaluate) {
        this.setProductCells();
        this.evaluate = evaluate;
        this.refGame.showText(this.refGame.global.resources.getTutorialText('train'));
        this.clear();
        let scenario = this.refGame.global.resources.getScenario();
        this.initGame(JSON.parse(scenario));
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    /**
     * Initie le début du jeu avec le scénario
     * @param scenario
     */
    initGame(scenario){
        if(this.evaluate){
            this.refGame.global.statistics.addStats(2);
        }

        this.activeProducts = [];
        let imgPyramid = this.refGame.global.resources.getImage("pyramide_vide").image.src;
        this.pyramid = new Pyramid(375,300,450,390, imgPyramid);
        this.activeProducts.push(this.pyramid);

        this.degre = this.refGame.global.resources.getDegre();

        this.categories = [];
        for (let i = 0; i < scenario.length; i++) {
            let categorie = scenario[i];
            this.categories.push(new Category(categorie.name, categorie.description, categorie.level));
        }

        this.getAllProducts(scenario);
        this.showExerciceProducts();
    }

    /**
     * Défini des cellules dans lesquelles seront placés les produits en début de partie
     */
    setProductCells(){
        this.cells = [];
        for (let i = 1; i < 7; i++) {
            this.cells.push(new Cell(35, 70 * i, 70, false))
        }
        for (let i = 1; i < 7; i++) {
            this.cells.push(new Cell(100, 70 * i, 70, false))
        }
        for (let i = 1; i < 4; i++) {
            this.cells.push(new Cell(175, 70 * i, 70, false))
        }
    }


    /**
     * Récupère tous les produits du scénario depuis la DB
     * @param scenario
     */
    getAllProducts(scenario){
        // Récupération de tous les produits depuis le scénario et ajout dans un tableau
        let isPlaced = false;
        let isWellPlaced = false;

        this.products = [];
        for(var i = 0; i < scenario.length; i++) {
            var obj = scenario[i];
            for(var j = 0; j < obj.products.length; j++) {
                var produit = obj.products[j];
                if(obj.level == -1){
                    isPlaced = true;
                    isWellPlaced = true;
                }
                this.products.push(new Product(50, 50, 70, 70, this.refGame.global.resources.getImage(produit.imgName), produit.imgName, produit.name, produit.kCal, obj.level, produit.advice, produit.recommandation, isPlaced, isWellPlaced, this.onShapeMove.bind(this), this.onShapeClick.bind(this)));
            }
        }
    }

    /**
     * Affiche les produits présent dans l'exercice
     */
    showExerciceProducts(){
        //Affichage des produits de l'exercice
        this.data = this.refGame.global.resources.getExercice();

        let exercice = this.shuffle(JSON.parse(this.data.exercice));

        for(var i = 0; i < exercice.length; i++) {
            var ob = exercice[i];
            if(ob.category.localeCompare("Infos") != 0){
                for(var j = 0; j < ob.products.length; j++) {
                    var produit = ob.products[j];
                    for(var k = 0; k < this.products.length; k++) {
                        if(produit.name == this.products[k].getImageName()){
                            let product = this.placeProduct(this.products[k]);
                            this.activeProducts.push(product);
                        }
                    }
                }
            }
        }
        this.setElements(this.activeProducts);
    }

    /**
     * Méthode utilisée pour rendre aléatoire l'affichage des produits de l'exercice
     * @param array
     * @returns {*}
     */
    shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }

    /**
     * Change la position d'un produit pour le placer dans une cellule précédemment créé
     * @param product
     * @returns {*}
     */
    placeProduct(product){
        //Place le produit sur la première place disponible
        for (let i = 0; i < this.cells.length; i++) {
            if(this.cells[i].isFull == false){
                let cell = this.cells[i];
                product.setX(cell.getX());
                product.setY(cell.getY());
                cell.setisFull(true);
                break;
            }
        }
        return product;
    }

    /**
     * Affiche le résultat final dès que la pyramide est compléter
     */
    showResult(){
        let greenCircle = this.refGame.global.resources.getImage('rond_vert');
        let redCircle = this.refGame.global.resources.getImage('rond_rouge');
        let nbProducts = this.activeProducts.length -1;
        let nbCorrectProd = 0;
        let result = 'result_0';
        let intResult = 0;

        for (let i = 0; i < this.activeProducts.length; i++) {

            if(this.activeProducts[i].isWellPlaced){
                let product = this.activeProducts[i];
                this.elements.push(new Circle(product.getX(), product.getY(), greenCircle, product.getHeight()));
                product.onMove = false;
                nbCorrectProd++;

            } else if(this.activeProducts[i].isWellPlaced == false){
                let product = this.activeProducts[i];
                this.elements.push(new Circle(product.getX(), product.getY(), redCircle, product.getHeight()));
                product.onMove = false;
            }
        }
        this.init();
        if(nbCorrectProd > (nbProducts / 4)){
            result = 'result_1';
            intResult += 1;
            if(nbCorrectProd > (nbProducts / 4 *3)){
                result = 'result_2';
                intResult += 1;
            }
        }
        this.refGame.global.util.showAlert('info', this.refGame.global.resources.getOtherText('result') + " : " + this.refGame.global.resources.getOtherText(result), this.refGame.global.resources.getOtherText('result'), undefined,  this.endGame(intResult));
    }

    /**
     * Ajoute le résultat dans la DB si le joueur est en mode évaluer
     * @param intResult
     */
    endGame(intResult){
        if(this.evaluate){
            this.refGame.global.statistics.updateStats(intResult);
        }

        this.end = true;
    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {

    }



    /**
     * Affiche un pop-up avec certaines informations sur l'aliment en fonction du niveau Harmos sélectionné
     * @param shape
     */
    onShapeClick(shape){

        if(shape.getCategory() == -1){
            this.refGame.global.util.showAlert('info',  "", shape.name);
        } else {
            if(this.degre < 3){
                this.refGame.global.util.showAlert('info',  shape.kCal + " " + this.refGame.global.resources.getOtherText('100_g') + ". " + shape.recommandation, shape.name, shape.advice);
            } else if(this.degre <7){
                this.refGame.global.util.showAlert('info',  shape.kCal + " " + this.refGame.global.resources.getOtherText('100_g'), shape.name, shape.advice);
            } else if(this.degre < 9){
                this.refGame.global.util.showAlert('info',  shape.kCal + " " + this.refGame.global.resources.getOtherText('100_g'), shape.name);
            } else{
                this.refGame.global.util.showAlert('info',  "", shape.name);
            }
        }

    }

    /**
     * Détecte sur quel niveau a été placé l'aliment et affiche une pop-up indiquant si le placement est correcte ou pas
     * Si tous les éléments sont placés, appelle la méthode showResult()
     * @param shape
     */
    onShapeMove(shape){
        let canvasHeight = this.refGame.global.canvas.HEIGHT;
        let canvasWidth = this.refGame.global.canvas.WIDTH;
        let pyramidFull = true;

        let leftMargin = (canvasWidth - this.pyramid.width);
        let bottomTopMargin = (canvasHeight - this.pyramid.height) / 2;
        let levelHeight = this.pyramid.getLevelHeight();

        let levelWidth = 37.5;
        let levelNumber = 5;

        if(shape.getCategory() == -1){
            shape.setisPlaced(true);
            shape.setisWellPlaced(true);
        } else{
            shape.setisPlaced(false);
            shape.setisWellPlaced(false);
        }

        // Boucle dans les catégories pour détecter sur quel niveau est présent le produit placé
        for (let i = 0; i < this.categories.length; i++) {
            if(shape.y < (canvasHeight - bottomTopMargin  - (levelHeight * i )) && shape.y > (bottomTopMargin + (levelHeight * (levelNumber - i))) && shape.x > leftMargin + (levelWidth* i) && shape.x < canvasWidth - (levelWidth * i)){
                if(shape.getCategory() == i + 1){
                    shape.setisPlaced(true);
                    shape.setisWellPlaced(true);
                } else {
                    shape.setisPlaced(true);
                    shape.setisWellPlaced(false);
                }
            }
        }
        for (let i = 0; i < this.activeProducts.length; i++) {
            if(this.activeProducts[i].isPlaced == false){
                pyramidFull = false;
                break;
            }
        }
        if(pyramidFull){
            this.showResult();
        }
    }
}
/**
 * @classdesc Représente un mode de jeu
 * @author Vincent Audergon
 * @version 1.0
 */
class Mode {

    /**
     * Constructeur d'un mode de jeu
     * @param {string} name le nom du mode de jeu (référencé dans le JSON de langues)
     */
    constructor(name) {
        this.name = name;
        this.interfaces = [];
        this.texts = [];
    }

    /**
     * Défini la liste des interfaces utilisées par le mode de jeu
     * @param {Interface[]} interfaces la liste des interfaces
     */
    setInterfaces(interfaces) {
        this.interfaces = interfaces;
    }

    /**
     * Ajoute une interface au mode de jeu
     * @param {string} name le nom de l'interface
     * @param {Interface} inter l'interface
     */
    addInterface(name, inter) {
        this.interfaces[name] = inter;
    }

    /**
     * Retourne un texte dans la langue courrante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key){
        return this.texts[key];
    }

    /**
     * Méthode de callback appelée lors d'un changement de langue.
     * Indique à toutes les interfaces du mode de jeu de changer les textes en fonction de la nouvelle langue
     * @param {string} lang la langue
     */
    onLanguageChanged(lang) {
        this.texts = this.refGame.global.resources.getOtherText(this.name)
        for (let i in this.interfaces) {
            let inter = this.interfaces[i];
            if (inter instanceof Interface) {
                inter.setTexts(this.texts);
                inter.refreshLang(lang);
            }
        }
    }

}

class CreateMode extends Mode{

    constructor(refGame){
        super('create');
        this.refGame = refGame;
        this.setInterfaces({
            create: new Create(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.create.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.create.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.create.refreshFont(isOpendyslexic)
    }
}

class ExploreMode extends Mode{

    constructor(refGame){
        super('explore');
        this.refGame = refGame;
        this.setInterfaces({
            explore: new Explore(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.explore.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.explore.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.explore.refreshFont(isOpendyslexic)
    }
}

class PlayMode extends Mode{

    constructor(refGame){
        super('play');
        this.refGame = refGame;
        this.setInterfaces({
            play: new Play(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.play.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.play.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.play.refreshFont(isOpendyslexic)
    }
}

class TrainMode extends Mode{

    constructor(refGame){
        super('train');
        this.refGame = refGame;
        this.evaluate = false;
        this.setInterfaces({
            train: new Train(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(evaluate){
        this.evaluate = evaluate;
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.train.show(this.evaluate);
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.train.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.train.refreshFont(isOpendyslexic)
    }
}
/**
 * @classdesc Classe principale du jeu
 * @author Vincent Audergon
 * @version 1.0
 */

const VERSION = 1.0;

class Game {

    /**
     * Constructeur du jeu
     * @param {Object} global Un objet contenant les différents objets du Framework
     */
    constructor(global) {
        this.global = global;
        this.global.util.callOnGamemodeChange(this.onGamemodeChanged.bind(this));
        this.global.resources.callOnLanguageChange(this.onLanguageChanged.bind(this));
        this.global.resources.callOnFontChange(this.onFontChange.bind(this));
        this.scenes = {
            explore:new PIXI.Container(),
            train:new PIXI.Container(),
            create:new PIXI.Container(),
            play:new PIXI.Container()
        };
        this.exploreMode = new ExploreMode(this);
        this.trainMode = new TrainMode(this);
        this.createMode = new CreateMode(this);
        this.playMode = new PlayMode(this);
        this.oldGamemode = undefined;
    }

    /**
     * Affiche une scène sur le canvas
     * @param {PIXI.Container} scene La scène à afficher
     */
    showScene(scene) {
        this.reset();
        this.global.pixiApp.stage.addChild(scene);
    }

    /**
     * Retire toutes les scènes du stage PIXI
     */
    reset() {
        for (let scene in this.scenes) {
            this.global.pixiApp.stage.removeChild(this.scenes[scene])
        }
    }

    /**
     * Affiche du texte sur la page (à gauche du canvas)
     * @param {string} text Le texte à afficher
     */
    showText(text) {
        this.global.util.setTutorialText(text);
    }

    /**
     * Fonction de callback appelée lors d'un chagement de langue.
     * Indiques à tous les modes de jeu le changement de langue
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.exploreMode.onLanguageChanged(lang);
        this.trainMode.onLanguageChanged(lang);
        this.createMode.onLanguageChanged(lang);
        this.playMode.onLanguageChanged(lang);
    }


    onFontChange(isOpendyslexic) {
        this.createMode.onFontChange(isOpendyslexic);
        this.exploreMode.onFontChange(isOpendyslexic);
        this.trainMode.onFontChange(isOpendyslexic);
        this.playMode.onFontChange(isOpendyslexic);
    }

    /**
     * Listener appelé par le Framework lorsque le mode de jeu change.
     * Démarre le bon mode de jeu en fonction de celui qui est choisi
     * @async
     */
    async onGamemodeChanged() {
        let gamemode = this.global.util.getGamemode();
        switch (gamemode) {
            case this.global.Gamemode.Evaluate:
                this.trainMode.init(true);
                this.trainMode.show();
                break;
            case this.global.Gamemode.Train:
                this.trainMode.init(false);
                this.trainMode.show();
                break;
            case this.global.Gamemode.Explore:
                this.exploreMode.init();
                this.exploreMode.show();
                break;
            case this.global.Gamemode.Create:
                this.createMode.init();
                this.createMode.show();
                break;
            case this.global.Gamemode.Play:
                this.playMode.init();
                this.playMode.show();
                break;
        }
        this.oldGamemode = gamemode;
    }
}
const global = {};
global.canvas = Canvas.getInstance();
global.Log = Log;
global.util = Util.getInstance();
global.pixiApp = new PixiFramework().getApp();
global.resources = Resources;
global.statistics = Statistics;
global.Gamemode = Gamemode;
global.listenerManager = ListenerManager.getInstance();
const game = new Game(global);
