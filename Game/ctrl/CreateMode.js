
class CreateMode extends Mode{

    constructor(refGame){
        super('create');
        this.refGame = refGame;
        this.setInterfaces({
            create: new Create(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.create.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.create.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.create.refreshFont(isOpendyslexic)
    }
}
