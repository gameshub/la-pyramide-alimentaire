class Category{

    /**
     * Constucteur des catégories permettant de séparer les aliments
     * @param name
     * @param description
     * @param level
     */
    constructor(name, description, level) {
        this._name = name;
        this._description = description;
        this._level = level;
    }


    getname() {
        return this._name;
    }

    setname(value) {
        this._name = value;
    }

    getdescription() {
        return this._description;
    }

    setdescription(value) {
        this._description = value;
    }

    getlevel() {
        return this._level;
    }

    setlevel(value) {
        this._level = value;
    }
}