/**
 * /**
 * @classdesc IHM pour le mode Explorer
 * @author Julien Savoy
 * @version 1.0
 */
class Explore extends Interface {

    /**
     * Consctucture de l'IHM Explorer
     * @param refGame
     */
    constructor(refGame) {
        super(refGame, "explore");

    }


    /**
     * Point d'entrée pour l'affichage
     */
    show() {
        this.setProductCells();
        this.clear();
        this.refGame.showText(this.refGame.global.resources.getTutorialText('explore'));
        let scenario = this.refGame.global.resources.getScenario();
        this.initGame(JSON.parse(scenario));
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }


    /**
     * Initie le début du jeu avec le scénario
     * @param scenario
     */
    initGame(scenario){
        this.activeProducts = [];

        let imgPyramid = this.refGame.global.resources.getImage("pyramide_vide_couleur").image.src;
        this.pyramid = new Pyramid(375,300,450,390, imgPyramid);
        this.activeProducts.push(this.pyramid);


        // Création des 6 catégories d'aliments
        this.categories = [];
        for (let i = 0; i < scenario.length; i++) {
            let categorie = scenario[i];
            this.categories.push(new Category(categorie.name, categorie.description, categorie.level));
        }

        this.getAllProducts(scenario);
        this.showExerciceProducts();
    }


    /**
     * Défini des cellules dans lesquelles seront placés les produits en début de partie
     */
    setProductCells(){
        this.cells = [];
        for (let i = 1; i < 7; i++) {
            this.cells.push(new Cell(35, 70 * i, 70, false))
        }
        for (let i = 1; i < 7; i++) {
            this.cells.push(new Cell(100, 70 * i, 70, false))
        }
        for (let i = 1; i < 4; i++) {
            this.cells.push(new Cell(175, 70 * i, 70, false))
        }
    }

    /**
     * Récupère tous les produits du scénario depuis la DB
     * @param scenario
     */
    getAllProducts(scenario){
        this.products = [];
        for(var i = 0; i < scenario.length; i++) {
            var obj = scenario[i];
            for(var j = 0; j < obj.products.length; j++) {
                var produit = obj.products[j];
                this.products.push(new Product(50, 50, 70, 70, this.refGame.global.resources.getImage(produit.imgName), produit.imgName, produit.name, produit.kCal, obj.level, produit.advice, produit.recommandation, false, false, this.onShapeMove.bind(this), this.onShapeClick.bind(this)));
            }
        }


    }

    /**
     * Affiche les produits présent dans l'exercice
     */
    showExerciceProducts(){
        this.data = this.refGame.global.resources.getExercice();

        let exercice = this.shuffle(JSON.parse(this.data.exercice));

        for(var i = 0; i < exercice.length; i++) {
            var ob = exercice[i];
            if(ob.category.localeCompare("Infos") != 0){
                for(var j = 0; j < ob.products.length; j++) {
                    var produit = ob.products[j];
                    for(var k = 0; k < this.products.length; k++) {
                        if(produit.name == this.products[k].getImageName()){
                            let product = this.placeProduct(this.products[k]);
                            this.activeProducts.push(product);
                        }
                   }
                }
            }
        }
        this.setElements(this.activeProducts);
    }

    /**
     * Méthode utilisée pour rendre aléatoire l'affichage des produits de l'exercice
     * @param array
     * @returns {*}
     */
    shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    /**
     * Change la position d'un produit pour le placer dans une cellule précédemment créé
     * @param product
     * @returns {*}
     */
    placeProduct(product){
        for (let i = 0; i < this.cells.length; i++) {
            if(this.cells[i].isFull == false){
                let cell = this.cells[i];
                product.setX(cell.getX());
                product.setY(cell.getY());
                cell.setisFull(true);
                break;
            }
        }
        return product;
    }

     /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {

    }


    /**
     * Affiche un pop-up avec toutes les informations sur l'aliment sélectionné
     * @param shape
     */
    onShapeClick(shape){
        if(shape.getCategory() == -1){
            this.refGame.global.util.showAlert('info',  "", shape.name);
        } else {
                this.refGame.global.util.showAlert('info',  shape.kCal + " " + this.refGame.global.resources.getOtherText('100_g') + ". " + shape.recommandation, shape.name, shape.advice);
        }
    }

    /**
     * Détecte sur quel niveau a été placé l'aliment et affiche une pop-up indiquant si le placement est correcte ou pas
     * S'il ne l'est pas, le produit reviens à sa dernière position
     * @param shape
     */
    onShapeMove(shape){
        let canvasHeight = this.refGame.global.canvas.HEIGHT;
        let canvasWidth = this.refGame.global.canvas.WIDTH;

        let leftMargin = (canvasWidth - this.pyramid.width);
        let bottomTopMargin = (canvasHeight - this.pyramid.height) / 2;
        let levelHeight = this.pyramid.getLevelHeight();

        let levelWidth = 37.5;
        let levelNumber = 5;

        // Boucle dans les catégories pour détecter sur quel niveau est présent le produit placé
        for (let i = 0; i < this.categories.length; i++) {
            if(shape.y < (canvasHeight - bottomTopMargin  - (levelHeight * i )) && shape.y > (bottomTopMargin + (levelHeight * (levelNumber - i))) && shape.x > leftMargin + (levelWidth * i) && shape.x < canvasWidth - (levelWidth * i)){
                if(shape.getCategory() == this.categories[i].getlevel()){
                    this.refGame.global.util.showAlert('success', this.refGame.global.resources.getOtherText('right_placement') + shape.name, this.refGame.global.resources.getOtherText('bravo'));
                } else {
                    this.refGame.global.util.showAlert('error', this.refGame.global.resources.getOtherText('product') + shape.name + this.refGame.global.resources.getOtherText('wrong_placement'), this.refGame.global.resources.getOtherText('error'));
                    shape.setX(shape.previousX);
                    shape.setY(shape.previousY);
                }
            }
        }
    }

}