/**
 * /**
 * @classdesc IHM pour les modes Entrainer & Evaluer
 * @author Julien Savoy
 * @version 1.0
 */
class Train extends Interface {

    /**
     * Consctucture de l'IHM Explorer
     * @param refGame
     */
    constructor(refGame) {

        super(refGame, "train");
        this.evaluate = false;
    }

    /**
     * Point d'entrée pour l'affichage
     */
    show(evaluate) {
        this.setProductCells();
        this.evaluate = evaluate;
        this.refGame.showText(this.refGame.global.resources.getTutorialText('train'));
        this.clear();
        let scenario = this.refGame.global.resources.getScenario();
        this.initGame(JSON.parse(scenario));
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    /**
     * Initie le début du jeu avec le scénario
     * @param scenario
     */
    initGame(scenario){
        if(this.evaluate){
            this.refGame.global.statistics.addStats(2);
        }

        this.activeProducts = [];
        let imgPyramid = this.refGame.global.resources.getImage("pyramide_vide").image.src;
        this.pyramid = new Pyramid(375,300,450,390, imgPyramid);
        this.activeProducts.push(this.pyramid);

        this.degre = this.refGame.global.resources.getDegre();

        this.categories = [];
        for (let i = 0; i < scenario.length; i++) {
            let categorie = scenario[i];
            this.categories.push(new Category(categorie.name, categorie.description, categorie.level));
        }

        this.getAllProducts(scenario);
        this.showExerciceProducts();
    }

    /**
     * Défini des cellules dans lesquelles seront placés les produits en début de partie
     */
    setProductCells(){
        this.cells = [];
        for (let i = 1; i < 7; i++) {
            this.cells.push(new Cell(35, 70 * i, 70, false))
        }
        for (let i = 1; i < 7; i++) {
            this.cells.push(new Cell(100, 70 * i, 70, false))
        }
        for (let i = 1; i < 4; i++) {
            this.cells.push(new Cell(175, 70 * i, 70, false))
        }
    }


    /**
     * Récupère tous les produits du scénario depuis la DB
     * @param scenario
     */
    getAllProducts(scenario){
        // Récupération de tous les produits depuis le scénario et ajout dans un tableau
        let isPlaced = false;
        let isWellPlaced = false;

        this.products = [];
        for(var i = 0; i < scenario.length; i++) {
            var obj = scenario[i];
            for(var j = 0; j < obj.products.length; j++) {
                var produit = obj.products[j];
                if(obj.level == -1){
                    isPlaced = true;
                    isWellPlaced = true;
                }
                this.products.push(new Product(50, 50, 70, 70, this.refGame.global.resources.getImage(produit.imgName), produit.imgName, produit.name, produit.kCal, obj.level, produit.advice, produit.recommandation, isPlaced, isWellPlaced, this.onShapeMove.bind(this), this.onShapeClick.bind(this)));
            }
        }
    }

    /**
     * Affiche les produits présent dans l'exercice
     */
    showExerciceProducts(){
        //Affichage des produits de l'exercice
        this.data = this.refGame.global.resources.getExercice();

        let exercice = this.shuffle(JSON.parse(this.data.exercice));

        for(var i = 0; i < exercice.length; i++) {
            var ob = exercice[i];
            if(ob.category.localeCompare("Infos") != 0){
                for(var j = 0; j < ob.products.length; j++) {
                    var produit = ob.products[j];
                    for(var k = 0; k < this.products.length; k++) {
                        if(produit.name == this.products[k].getImageName()){
                            let product = this.placeProduct(this.products[k]);
                            this.activeProducts.push(product);
                        }
                    }
                }
            }
        }
        this.setElements(this.activeProducts);
    }

    /**
     * Méthode utilisée pour rendre aléatoire l'affichage des produits de l'exercice
     * @param array
     * @returns {*}
     */
    shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }

    /**
     * Change la position d'un produit pour le placer dans une cellule précédemment créé
     * @param product
     * @returns {*}
     */
    placeProduct(product){
        //Place le produit sur la première place disponible
        for (let i = 0; i < this.cells.length; i++) {
            if(this.cells[i].isFull == false){
                let cell = this.cells[i];
                product.setX(cell.getX());
                product.setY(cell.getY());
                cell.setisFull(true);
                break;
            }
        }
        return product;
    }

    /**
     * Affiche le résultat final dès que la pyramide est compléter
     */
    showResult(){
        let greenCircle = this.refGame.global.resources.getImage('rond_vert');
        let redCircle = this.refGame.global.resources.getImage('rond_rouge');
        let nbProducts = this.activeProducts.length -1;
        let nbCorrectProd = 0;
        let result = 'result_0';
        let intResult = 0;

        for (let i = 0; i < this.activeProducts.length; i++) {

            if(this.activeProducts[i].isWellPlaced){
                let product = this.activeProducts[i];
                this.elements.push(new Circle(product.getX(), product.getY(), greenCircle, product.getHeight()));
                product.onMove = false;
                nbCorrectProd++;

            } else if(this.activeProducts[i].isWellPlaced == false){
                let product = this.activeProducts[i];
                this.elements.push(new Circle(product.getX(), product.getY(), redCircle, product.getHeight()));
                product.onMove = false;
            }
        }
        this.init();
        if(nbCorrectProd > (nbProducts / 4)){
            result = 'result_1';
            intResult += 1;
            if(nbCorrectProd > (nbProducts / 4 *3)){
                result = 'result_2';
                intResult += 1;
            }
        }
        this.refGame.global.util.showAlert('info', this.refGame.global.resources.getOtherText('result') + " : " + this.refGame.global.resources.getOtherText(result), this.refGame.global.resources.getOtherText('result'), undefined,  this.endGame(intResult));
    }

    /**
     * Ajoute le résultat dans la DB si le joueur est en mode évaluer
     * @param intResult
     */
    endGame(intResult){
        if(this.evaluate){
            this.refGame.global.statistics.updateStats(intResult);
        }

        this.end = true;
    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {

    }



    /**
     * Affiche un pop-up avec certaines informations sur l'aliment en fonction du niveau Harmos sélectionné
     * @param shape
     */
    onShapeClick(shape){

        if(shape.getCategory() == -1){
            this.refGame.global.util.showAlert('info',  "", shape.name);
        } else {
            if(this.degre < 3){
                this.refGame.global.util.showAlert('info',  shape.kCal + " " + this.refGame.global.resources.getOtherText('100_g') + ". " + shape.recommandation, shape.name, shape.advice);
            } else if(this.degre <7){
                this.refGame.global.util.showAlert('info',  shape.kCal + " " + this.refGame.global.resources.getOtherText('100_g'), shape.name, shape.advice);
            } else if(this.degre < 9){
                this.refGame.global.util.showAlert('info',  shape.kCal + " " + this.refGame.global.resources.getOtherText('100_g'), shape.name);
            } else{
                this.refGame.global.util.showAlert('info',  "", shape.name);
            }
        }

    }

    /**
     * Détecte sur quel niveau a été placé l'aliment et affiche une pop-up indiquant si le placement est correcte ou pas
     * Si tous les éléments sont placés, appelle la méthode showResult()
     * @param shape
     */
    onShapeMove(shape){
        let canvasHeight = this.refGame.global.canvas.HEIGHT;
        let canvasWidth = this.refGame.global.canvas.WIDTH;
        let pyramidFull = true;

        let leftMargin = (canvasWidth - this.pyramid.width);
        let bottomTopMargin = (canvasHeight - this.pyramid.height) / 2;
        let levelHeight = this.pyramid.getLevelHeight();

        let levelWidth = 37.5;
        let levelNumber = 5;

        if(shape.getCategory() == -1){
            shape.setisPlaced(true);
            shape.setisWellPlaced(true);
        } else{
            shape.setisPlaced(false);
            shape.setisWellPlaced(false);
        }

        // Boucle dans les catégories pour détecter sur quel niveau est présent le produit placé
        for (let i = 0; i < this.categories.length; i++) {
            if(shape.y < (canvasHeight - bottomTopMargin  - (levelHeight * i )) && shape.y > (bottomTopMargin + (levelHeight * (levelNumber - i))) && shape.x > leftMargin + (levelWidth* i) && shape.x < canvasWidth - (levelWidth * i)){
                if(shape.getCategory() == i + 1){
                    shape.setisPlaced(true);
                    shape.setisWellPlaced(true);
                } else {
                    shape.setisPlaced(true);
                    shape.setisWellPlaced(false);
                }
            }
        }
        for (let i = 0; i < this.activeProducts.length; i++) {
            if(this.activeProducts[i].isPlaced == false){
                pyramidFull = false;
                break;
            }
        }
        if(pyramidFull){
            this.showResult();
        }
    }
}