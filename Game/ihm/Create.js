/**
 * /**
 * @classdesc IHM pour le mode Créer
 * @author Julien Savoy
 * @version 1.0
 */
class Create extends Interface {

    /**
     * Consctucture de l'IHM Create
     * @param refGame
     */
    constructor(refGame) {

        super(refGame, "create");

    }

    /**
     * Point d'entrée pour l'affichage
     */
    show() {
        this.clear();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    /**
     * /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {

    }



}