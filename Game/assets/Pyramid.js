class Pyramid extends Asset {


    /**
     * Constucteur de la pyramide utilisée pendant le jeu
     * @param x
     * @param y
     * @param width
     * @param height
     * @param imgBackground
     */
    constructor(x, y, width, height, imgBackground) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.levelHeight = height / 6;
        this.imgBackground = imgBackground;

        this.container = new PIXI.Container();

        this.init();
    }


    init(){
        this.container.removeChildren();

        let bg = PIXI.Sprite.fromImage(this.imgBackground);
        bg.x = this.x;
        bg.y = this.y;
        bg.width = this.width;
        bg.height = this.height;
        bg.anchor.x = 0.5;
        bg.anchor.y = 0.5;

        this.container.addChild(bg);
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    getLevelHeight(){
        return this.levelHeight;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    get isFull() {
        return this.isFull;
    }

    set isFull(value) {
        this.isFull = value;
    }

}