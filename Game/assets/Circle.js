class Circle extends Asset {


    /**
     * Constucteur des cercles indiquant quels produits est bien ou mal placés
     * @param x
     * @param y
     * @param bgImage
     * @param size
     */
    constructor(x, y, bgImage, size){
        super();
        this.x = x;
        this.y = y;
        this.bgImage = bgImage;
        this.size = size;

        this.container = new PIXI.Container();

        this.init();

    }

    init(){
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        //Créer un "Sprite" avec une image en base64.
        //Le type d'image est "IMG" donc pour récupérer l'image, il faut faire
        //this.image.image.src
        this.shape = PIXI.Sprite.fromImage(this.bgImage.image.src);

        //Définir sa position et l'ancre sur l'image, ici au centre de l'image
        this.shape.anchor.x = 0.5;
        this.shape.anchor.y = 0.5;
        this.shape.x = this.x;
        this.shape.y = this.y;
        this.shape.width = this.size;
        this.shape.height = this.size;

        this.container.addChild(this.shape);
    }


    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.size;
    }

    getHeight() {
        return this.size;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }


}