class Product extends Asset {


    /**
     * Constucteur des produits affichés pendant le jeu
     * @param x
     * @param y
     * @param width
     * @param height
     * @param image
     * @param imageName
     * @param name
     * @param kCal
     * @param category
     * @param advice
     * @param recommandation
     * @param isPlaced
     * @param isWellPlaced
     * @param onMove
     * @param onDBClick
     */
    constructor(x, y, width, height, image, imageName, name, kCal, category, advice, recommandation, isPlaced, isWellPlaced, onMove, onDBClick) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.image = image;
        this.imageName = imageName;
        this.name = name;
        this.kCal = kCal;
        this.category = category;
        this.advice = advice;
        this.recommandation = recommandation;
        this.isPlaced = isPlaced;
        this.isWellPlaced = isWellPlaced;
        this.onMove = onMove;
        this.onDBClick = onDBClick;

        this.container = new PIXI.Container();
        this.isFirstClick = true;
        this.data = undefined;
        this.shape = undefined;
        this.display();
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        //Créer un "Sprite" avec une image en base64.
        //Le type d'image est "IMG" donc pour récupérer l'image, il faut faire
        //this.image.image.src
        this.shape = PIXI.Sprite.fromImage(this.image.image.src);

        //Définir sa position et l'ancre sur l'image, ici au centre de l'image
        this.shape.anchor.x = 0.5;
        this.shape.anchor.y = 0.5;
        this.shape.x = this.x;
        this.shape.y = this.y;
        this.shape.width = this.width;
        this.shape.height = this.height;

        //Ajouter les listeners
        this.shape.interactive = true;
        this.shape.on('pointerdown', this.onClick.bind(this));
        this.shape.on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));

        //Ajouter notre forme sur l'élément graphique
        this.container.addChild(this.shape);
    }

    onClick() {
        if (this.isFirstClick) {
            this.isFirstClick = false;
            //Annulation du double clique après 500ms
            setTimeout(function () {
                this.isFirstClick = true;
            }.bind(this), 500);
        } else {
            this.isFirstClick = true;
            if (this.onDBClick)
                this.onDBClick(this);
        }
    }

    onDragStart(event) {
        if(this.onMove == false){

        } else{
            this.data = event.data;
            this.previousX = this.x;
            this.previousY = this.y;
        }
    }


    onDragEnd() {
        this.data = null;
        //Appel au callback s'il y en a un
        if (this.onMove) {
            this.onMove(this);
        }
    }

    onDragMove() {
        if (this.data) {
            //Position actuelle
            let newPosition = this.data.getLocalPosition(this.shape.parent);

            //Empêcher la forme de sortir du canvas et de devenir invisible
            newPosition.x = newPosition.x < 0 ? 0 : newPosition.x;
            newPosition.y = newPosition.y < 0 ? 0 : newPosition.y;
            newPosition.x = newPosition.x > 600 ? 600 : newPosition.x;
            newPosition.y = newPosition.y > 600 ? 600 : newPosition.y;

            //Mise à jour la position de la forme
            this.shape.x = newPosition.x;
            this.shape.y = newPosition.y;
            this.y = newPosition.y;
            this.x = newPosition.x;

        }
    }


    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        //Mise à jour de l'affichage
        this.display();
    }

    setX(x) {
        this.x = x;
        //Mise à jour de l'affichage
        this.display();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    getImageName() {
        return this.imageName;
    }

    getName() {
        return this.name;
    }

    getKcal() {
        return this.kCal;
    }

    getCategory() {
        return this.category;
    }

    getAdvice() {
        return this.advice;
    }

    getRecommandation() {
        return this.recommandation;
    }

    getisPlaced(){
        return this.isPlaced;
    }

    setisPlaced(isPlaced){
        this.isPlaced = isPlaced;
    }

    getisWellPlaced(){
        return this.isWellPlaced;
    }

    setisWellPlaced(isWellPlaced){
        this.isWellPlaced = isWellPlaced;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }
}