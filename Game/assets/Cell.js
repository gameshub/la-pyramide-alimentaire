class Cell extends Asset {


    /**
     * Constucteur des cellules servant à placer les produits en début de partie
     * @param x
     * @param y
     * @param size
     * @param isFull
     */
    constructor(x, y, size, isFull){
        super();
        this.x = x;
        this.y = y;
        this.size = size;
        this.isFull = isFull;

        this.container = new PIXI.Container();

    }


    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        this.init();
    }

    setX(x) {
        this.x = x;
        this.init();
    }

    getWidth() {
        return this.size;
    }

    getHeight() {
        return this.size;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }


    getisFull() {
        return this.isFull;
    }

    setisFull(value) {
        this.isFull = value;
    }
}